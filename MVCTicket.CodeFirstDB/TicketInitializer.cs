﻿using MVCTicket.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCTicket.CodeFirstDB
{
    class TicketInitializer : DropCreateDatabaseAlways<TicketContext>
    {
        
        protected override void Seed(TicketContext context)
        {
            var p = new List<Project>
            {
                new Project{ Name = "Project 1"},
                new Project{ Name = "Project 2"},
                new Project{ Name = "Project 3"},
                new Project{ Name = "Project 4"},
                new Project{ Name = "Project 5"},
                new Project{ Name = "Project 6"},
                new Project{ Name = "Project 7"},
                new Project{ Name = "Project 8"},
            };
            p.ForEach(c => context.Projects.Add(c));
            context.SaveChanges();

            var t = new List<Ticket>
            {
                new Ticket{ Title = "Title 1", Project= context.Projects.Single(s=>s.Name == "Project 1"),State = State.UnStarted },
                new Ticket{ Title = "Title 2", Project= context.Projects.Single(s=>s.Name == "Project 1"),State = State.UnStarted },
                new Ticket{ Title = "Title 3", Project= context.Projects.Single(s=>s.Name == "Project 1"),State = State.UnStarted },
                new Ticket{ Title = "Title 4", Project= context.Projects.Single(s=>s.Name == "Project 2"),State = State.UnStarted },
                new Ticket{ Title = "Title 5", Project= context.Projects.Single(s=>s.Name == "Project 2"),State = State.UnStarted },
                new Ticket{ Title = "Title 6", Project= context.Projects.Single(s=>s.Name == "Project 2"),State = State.UnStarted },
                new Ticket{ Title = "Title 7", Project= context.Projects.Single(s=>s.Name == "Project 3"),State = State.UnStarted },
                new Ticket{ Title = "Title 8", Project= context.Projects.Single(s=>s.Name == "Project 4"),State = State.UnStarted },
                new Ticket{ Title = "Title 9", Project= context.Projects.Single(s=>s.Name == "Project 5"),State = State.UnStarted },
                new Ticket{ Title = "Title 10", Project= context.Projects.Single(s=>s.Name == "Project 3"),State = State.UnStarted },
                new Ticket{ Title = "Title 11", Project= context.Projects.Single(s=>s.Name == "Project 3"),State = State.UnStarted },
                new Ticket{ Title = "Title 12", Project= context.Projects.Single(s=>s.Name == "Project 5"),State = State.UnStarted },
                new Ticket{ Title = "Title 13", Project= context.Projects.Single(s=>s.Name == "Project 6"),State = State.UnStarted },
                new Ticket{ Title = "Title 14", Project= context.Projects.Single(s=>s.Name == "Project 7"),State = State.UnStarted },
                new Ticket{ Title = "Title 16", Project= context.Projects.Single(s=>s.Name == "Project 8"),State = State.UnStarted },
            };
            t.ForEach(a => context.Tickets.Add(a));
            context.SaveChanges();
        }
            
    }
}
