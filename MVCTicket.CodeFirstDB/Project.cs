﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace MVCTicket.Models
{
    public class Project
    {
        [Key]
        public int ProjectId { get; set; }

        [Display(Name="Project Name")]
        [Required(ErrorMessage="Project must have Name!")]
        [MaxLength(500)]
        public string Name { get; set; }
    }
}