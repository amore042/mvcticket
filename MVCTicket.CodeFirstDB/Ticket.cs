﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVCTicket.Models
{
    public enum State
    {
        UnStarted, Started, Finished, Accepted, Rejected
    }

    public enum TicketType
    {
        Bug, Feature
    }

    public class Ticket
    {
        public Ticket()
        {
            State = Models.State.UnStarted;
            Created = DateTime.Now;
        }

        [Key]
        public int TicketId { get; set; }

        [Required(ErrorMessage = "Title Must be filled!")]
        [MaxLength(2048)]
        public string Title { get; set; }

        public string Discription { get; set; }

        public State State { get; set; }

        [Required(ErrorMessage = "Type Must be filled!")]
        public TicketType Type { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime Created { get; set; }

        [DisplayFormat(DataFormatString = "{0:g}", ApplyFormatInEditMode = true)]
        public DateTime? Started { get; set; }
        [DisplayFormat(DataFormatString = "{0:g}", ApplyFormatInEditMode = true)]
        public DateTime? Finished { get; set; }


        [MaxLength(128)]
        public string UserId { get; set; }

        public virtual Project Project { get; set; }
        public virtual IEnumerable<Comment> Comments { get; set; }
    }
}