﻿using MVCTicket.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCTicket.CodeFirstDB
{
    public class TicketContext :DbContext
    {
        public TicketContext()
            : base(@"Data Source=amol-pc\sqlexpress;Initial Catalog=MVCTicketDB2;Integrated Security=True")
        {

           
           
        }

        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Project> Projects { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

      
    }
}
