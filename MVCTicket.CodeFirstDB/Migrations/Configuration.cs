namespace MVCTicket.CodeFirstDB.Migrations
{
    using MVCTicket.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MVCTicket.CodeFirstDB.TicketContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(MVCTicket.CodeFirstDB.TicketContext context)
        {
            context.Projects.AddOrUpdate(
                foo => foo.Name,
                new Project { Name = "MVCTicket" }
                );
            context.SaveChanges();

            context.Tickets.AddOrUpdate(
                foo => foo.Title,
                new Ticket { Title = "Add user ID", Created = DateTime.Now, Project = context.Projects.Single(p => p.Name == "MVCTicket"), State = State.UnStarted, Type = TicketType.Feature, UserId = "Automated", Discription = "Add user who created the ticket to it." }
                );

            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
