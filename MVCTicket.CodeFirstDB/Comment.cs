﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVCTicket.Models
{
    public enum CommentType
    {
        Text, URL, GITHASH, Image
    }
    public class Comment
    {
        public Comment() 
        {
           // CreatedBy = User.Identity.GetUserId();
        }

        [Key]
        public int CommentId { get; set; }

        [Required(ErrorMessage="Cant submit a comment without any text!")]
        [MaxLength(2048)]
        public string Text { get; set; }

        [Required(ErrorMessage = "Cant submit a comment without selecting type!")]
        public CommentType Type { get; set; }

        public int CreatedBy { get; set; }

        //foreign key
        public int TicketId { get; set; }
        
        public virtual Ticket Ticket { get; set; }
    }
}