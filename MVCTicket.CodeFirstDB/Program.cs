﻿using MVCTicket.CodeFirstDB.Migrations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCTicket.CodeFirstDB
{
    class Program
    {
        static void Main(string[] args)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<TicketContext, Configuration>());

            using (var db = new TicketContext())
            {
                //db.Blogs.Add(new Blog { Name = "Another Blog " });
                db.SaveChanges();

                //foreach (var blog in db.Blogs)
                //{
                //    Console.WriteLine(blog.Name);
                //}
            }

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        } 
    }
}
