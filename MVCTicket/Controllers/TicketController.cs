﻿using MVCTicket.CodeFirstDB;
using MVCTicket.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace MVCTicket.Controllers
{
    [Authorize]
    public class TicketController : Controller
    {
        public TicketContext db = new TicketContext();
        // GET: Ticket
        public ActionResult Index()
        {
            return View(db.Tickets.ToList());
        }

        public ActionResult Create()
        {
            TicketViewModel newTicket = new TicketViewModel();
            newTicket.ProjectList = new SelectList(db.Projects, "ProjectId", "Name");//from g in db.Projects select new Project { ProjectId = g.ProjectId("Key"), Name = g.Name("Display") };
            return View(newTicket);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public RedirectResult Create(TicketViewModel model)
        {
            Ticket newTicket = new Ticket();
            newTicket.Title = model.Title;
            newTicket.Type = model.Type;
            newTicket.State = model.State;
            newTicket.Discription = model.Discription;
            newTicket.UserId = User.Identity.GetUserId();

            Project p = db.Projects.Single(foo => foo.ProjectId == model.ProjectId);
            newTicket.Project = p;

            db.Tickets.Add(newTicket);
            db.SaveChanges();

            return Redirect("~/Ticket/Index");
        }



        public RedirectResult TicketStarted(int ticketId)
        {
            Ticket newTicket = db.Tickets.Single(foo => foo.TicketId == ticketId);
            newTicket.Started = DateTime.Now;
            newTicket.State = State.Started;
            db.SaveChanges();
            return Redirect("~/Ticket/Index");
        }



        public RedirectResult TicketFinished(int ticketId)
        {
            Ticket newTicket = db.Tickets.Single(foo => foo.TicketId == ticketId);
            newTicket.Finished = DateTime.Now;
            newTicket.State = State.Finished;
            db.SaveChanges();
            return Redirect("~/Ticket/Index");
        }

        public ActionResult Delete(int ticketId)
        {
            Ticket ticketModel = db.Tickets.Single(foo => foo.TicketId == ticketId);
            return View(ticketModel);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int ticketId)
        {
            Ticket ticketModel = db.Tickets.Single(foo => foo.TicketId == ticketId);
            db.Tickets.Remove(ticketModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

    }
}