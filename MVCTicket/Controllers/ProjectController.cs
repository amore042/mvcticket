﻿using MVCTicket.CodeFirstDB;
using MVCTicket.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCTicket.Controllers
{
    [Authorize]
    public class ProjectController : Controller
    {
        public TicketContext db = new TicketContext();
        // GET: Project
        public ActionResult Index()
        {
            return View(db.Projects.ToList());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProjectViewModel model)
        {
            Project newProject = new Project();
            newProject.Name = model.Name;
            db.Projects.Add(newProject);
            db.SaveChanges();
            return View("Index",db.Projects.ToList());
        }
    }
}