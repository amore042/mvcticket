﻿using GridMvc.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCTicket.Models
{
    public class TicketViewModel
    {
        public TicketViewModel()
        {
            State = Models.State.UnStarted;
            Created = DateTime.Now;
        }

        [Key]
        public int TicketId { get; set; }

        [GridColumn(Title = "Title")]
        [Required(ErrorMessage="Title Must be filled!")]
        [MaxLength(2048)]
        public string Title { get; set; }
        
        [Required(ErrorMessage = "Title Must be filled!")]
        [MaxLength(3048)]
        public string Discription { get; set; }

        public State State { get; set; }

        [Required(ErrorMessage = "Type Must be filled!")]
        public TicketType Type { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Created { get; set; }

        public DateTime? Started { get; set; }
        public DateTime? Finished { get; set; }

        public int ProjectId { get; set; }

        public string User { get; set; }

        public SelectList ProjectList { get; set; }
    }
}